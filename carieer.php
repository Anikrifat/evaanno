<?php
include('inc/header.php');
?>
<main>
<section class="page-title page-title-layout1 text-center bg-overlay bg-overlay-gradient bg-parallax" style="background-image: url('assets/images/services/carieer.png'); background-size: cover; background-position: center center;">
      
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h1 class="pagetitle__heading mb-0">Careers</h1>
          </div><!-- /.col-xl-6 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section>

    <section class="careers">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-6 offset-lg-3">
            <div class="heading text-center mb-50">
              <h2 class="heading__subtitle color-body">We Prepare For The Future.</h2>
              <h3 class="heading__title">Inspire And Get Inspired By Professional Experts</h3>
            </div><!-- /.heading -->
          </div><!-- /.col-lg-10 -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="jobs-container">
              <!-- career item #1 -->
              <div class="job-item">
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="job__meta">
                      <span class="job__type">Full Time</span>
                      <span class="job__location">New York</span>
                    </div>
                    <h4 class="job__title">Chief Executive Officer</h4>
                  </div><!-- /.col-lg-4 -->
                  <div class="col-sm-12 col-md-12 col-lg-5">
                    <p class="job__desc">A chief executive officer (CEO) is the highest-ranking executive in a
                      company, and their primary responsibilities include making major corporate decisions.</p>
                  </div><!-- /.col-lg-5 -->
                  <div class="col-sm-12 col-md-12 col-lg-3 d-flex align-items-center justify-content-end btn-wrap">
                    <a href="#" class="btn btn__secondary">Apply Now</a>
                  </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
              </div><!-- /.job-item -->
              <!-- career item #2 -->
              <div class="job-item">
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="job__meta">
                      <span class="job__type">Full Time</span>
                      <span class="job__location">San Francisco</span>
                    </div>
                    <h4 class="job__title">Chief Financial Officer</h4>
                  </div><!-- /.col-lg-4 -->
                  <div class="col-sm-12 col-md-12 col-lg-5">
                    <p class="job__desc">A chief executive officer (CEO) is the highest-ranking executive in a
                      company, and their primary responsibilities include making major corporate decisions.</p>
                  </div><!-- /.col-lg-5 -->
                  <div class="col-sm-12 col-md-12 col-lg-3 d-flex align-items-center justify-content-end btn-wrap">
                    <a href="#" class="btn btn__secondary">Apply Now</a>
                  </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
              </div><!-- /.job-item -->
              <!-- career item #3 -->
              <div class="job-item">
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="job__meta">
                      <span class="job__type">Full Time</span>
                      <span class="job__location">New York</span>
                    </div>
                    <h4 class="job__title">Global Sales &amp; Marketing</h4>
                  </div><!-- /.col-lg-4 -->
                  <div class="col-sm-12 col-md-12 col-lg-5">
                    <p class="job__desc">A chief executive officer (CEO) is the highest-ranking executive in a
                      company, and their primary responsibilities include making major corporate decisions.</p>
                  </div><!-- /.col-lg-5 -->
                  <div class="col-sm-12 col-md-12 col-lg-3 d-flex align-items-center justify-content-end btn-wrap">
                    <a href="#" class="btn btn__secondary">Apply Now</a>
                  </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
              </div><!-- /.job-item -->
            </div>
          </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->
        <div class="row">
          <div class="col-12 text-center">
            <p class="text__link mt-30 mb-0">Pay Us A Visit And Have A Cup Of Coffee.
              <a href="industries.html" class="btn btn__secondary btn__link mx-1">
                <span>Get Started!</span> <i class="icon-arrow-right icon-outlined"></i>
              </a>
            </p>
          </div><!-- /.col-12 -->
        </div><!-- /.row -->
      </div><!-- /.container -->
    </section>
</main>
<?php
include('inc/footer.php');
?>