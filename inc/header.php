<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from 7oroof.com/demos/datasoft/home-classic# by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 26 Jun 2021 15:52:26 GMT -->

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta name="description" content="DataSoft  IT Solutions &  Services Template">
  <link href="assets/images/favicon/favicon.png" rel="icon">
  <title>Evanno</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@400;500;600;700&amp;family=Roboto:wght@400;700&amp;display=swap">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.css">
  <link rel="stylesheet" href="assets/css/libraries.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/new-style.css">
</head>

<body>
  <div class="wrapper">
    <!-- <div class="preloader">
            <div class="loading"><span></span><span></span><span></span><span></span></div>
        </div> -->
    <!-- /.preloader -->

    <!-- =========================
        Header
    =========================== -->
    <header class="header header-light">
      <div class="header__topbar">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="d-flex align-items-center justify-content-end">
                <!-- <ul class="contact__list d-flex flex-wrap align-items-center list-unstyled mb-0">
                  <li>
                    <i class="icon-phone"></i><a href="tel:+5565454117">+55 654 541 17</a>
                  </li>
                  <li>
                    <i class="icon-mail"></i><a href="mailto:Datasoft@7oroof.com">Datasoft@7oroof.com</a>
                  </li>
                  <li>
                    <i class="icon-clock"></i><a href="contact-us#">Hours: Mon-Fri: 8am – 7pm</a>
                  </li> -->
                </ul>
                <ul class="list-unstyled d-flex justify-content-end align-items-center mb-0">
                <li>
                    <div class="dropdown lang-dropdown">
                      <button class="dropdown-toggle lang-dropdown-toggle globe" id="langDropdown" data-toggle="dropdown">
                      <i class="icon-map"></i><span>Country</span>
                      </button>
                      <div class="dropdown-menu" aria-labelledby="langDropdown">
                        <a class="dropdown-item" href="#">Spain</a>
                        <a class="dropdown-item" href="#">France</a>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="dropdown lang-dropdown">
                      <button class="dropdown-toggle lang-dropdown-toggle" id="langDropdown" data-toggle="dropdown">
                        <i class="fas fa-globe"></i><span>English</span>
                      </button>
                      <div class="dropdown-menu" aria-labelledby="langDropdown">
                        <a class="dropdown-item" href="#">Spain</a>
                        <a class="dropdown-item" href="#">France</a>
                      </div>
                    </div>
                  </li>
                  <li>
                    <ul class="navbar-actions list-unstyled mb-0 d-flex align-items-center">
                      <li>
                        <button class="action__btn action__btn-login open-login-popup">
                          <i class="icon-user"></i><span>Login</span>
                        </button>
                      </li>
                      <li>
                        <button class="action__btn action__btn-login open-register-popup">
                          <i class="fas fa-user-plus"></i><span>register</span>
                        </button>
                      </li>
                    </ul>
                    <!-- /.navbar-actions -->
                  </li>
                </ul>
              </div>
            </div>
            <!-- /.col-12 -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.header-top -->
      <nav class="navbar navbar-expand-lg sticky-navbar">
        <div class="container">
          <a class="navbar-brand" href="index.php">
            <!-- <img src="assets/images/logo/logo-light.png" class="logo-light" alt="logo"> -->
            <img src="assets/images/logo/logo-dark.png" class="logo-dark img-fluid" alt="logo">
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>
          <div class="collapse navbar-collapse" id="mainNavigation">
            <ul class="navbar-nav mx-auto">
              <li class="nav__item">
                <a href="# data-toggle="dropdown" class="nav__item-link active">Home</a>

              </li>

              <!-- /.nav-item -->
              <li class="nav__item has-dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav__item-link">Ship</a>
                <ul class="dropdown-menu">
                  <li class="nav__item">
                    <a href="about-us#" class="nav__item-link">Express Services</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="why-us#" class="nav__item-link">Freight Services</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="leadership-team#" class="nav__item-link">Check Shipping Rates</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="awards#" class="nav__item-link">Prepare Shipment</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="awards#" class="nav__item-link">Schedule Pickup</a>
                  </li>
                </ul>
                <!-- /.dropdown-menu -->
              </li>
              <!-- /.nav-item -->
              <li class="nav__item has-dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav__item-link">Solutions & Services</a>
                <ul class="dropdown-menu">
                  <li class="nav__item">
                    <a href="about-us#" class="nav__item-link">Logistics</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="why-us#" class="nav__item-link">Drop and Ship</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="leadership-team#" class="nav__item-link">Shop & Ship</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="awards#" class="nav__item-link">Information Management</a>
                  </li>

                </ul>
                <!-- /.dropdown-menu -->
              </li>

              <li class="nav__item has-dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav__item-link">Help & Support</a>
                <ul class="dropdown-menu">
                  <li class="nav__item">
                    <a href="about-us#" class="nav__item-link">Customer Support</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="why-us#" class="nav__item-link">Find office</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="leadership-team#" class="nav__item-link">About Us</a>
                  </li>
                  <!-- /.nav-item -->
                  <li class="nav__item">
                    <a href="awards#" class="nav__item-link">FAQ</a>
                  </li>

                </ul>
                <!-- /.dropdown-menu -->
              </li>
              <!-- /.nav-item -->
              <li class="nav__item">
                <a href="carieer.php" class="nav__item-link">Carieer</a>
              </li>
              <!-- /.nav-item -->
              <!-- /.nav-item -->
              <li class="nav__item">
                <a href="contact-us#" class="nav__item-link">Contacts</a>
              </li>
              <!-- /.nav-item -->
            </ul>
            <!-- /.navbar-nav -->
            <button class="close-mobile-menu d-block d-lg-none"><i class="fas fa-times"></i></button>
          </div>
          <!-- /.navbar-collapse -->
          <div class="d-flex align-items-center">
            <!-- <a href="request-quote#" class="btn text-warning bg-white d-none d-xl-block ml-30">Login</a> -->
            <a href="#" class="action__btn action__btn-search ml-20"><i class="icon-search"></i></a>
          </div>
        </div>
        <!-- /.container -->
      </nav>
      <!-- /.navabr -->
    </header>
    <!-- /.Header -->