<?php
include('inc/header.php');
?>
<main>
    <!-- ============================
        Slider
    ============================== -->
    <section class="slider">
        <div class="m-slides-0">
            <div class="slide-item align-v-h bg-overlay bg-overlay-gradient">
                <div class="bg-img"><img src="assets/images/sliders/6.jpg" alt="slide img"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
                            <h1 class="pagetitle__heading">Want to Track Your Order?</h1>

                            <form class="pagetitle__form">
                                <div class="input-group mb-3 p-1 bg-white">
                                    <input type="text" class="form-control" placeholder="Enter Tracking Number....">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><button
                                                class="btn btn-warning text-white p-2 px-3">Track <i
                                                    class="fas fa-arrow-right"></i></button></span>
                                    </div>
                                </div>

                            </form>
                            <!--<p class="pagetitle__desc text-white p-3">We are experienced professionals who understand that It services is changing, and
                                are true partners who care about your success. </p>
                        </div><!-- /.col-xl-6 -->
                        </div><!-- /.row -->
                    </div>
                </div>

            </div>
    </section>
    <!-- /.slider -->
    <!-- ========================
      News
    =========================== -->

    <section class="news-service py-2">

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-l-12 col-xl-12">
                    <div class="heading mb-50">

                        <h2 class="heading__subtitle">
                            <marquee behavior="" direction="">News Alert</marquee>
                        </h2>
                    </div>
                </div><!-- /.col-xl-6 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="contact-panel">
                        <div class="content">
                            <h6 class="contact-panel__title">Manage Shipments</h6>
                            <ul class="list-item list-unstyled">
                                <li><a href="#"><i class="fas fa-pencil-alt mr-3"></i>Prepare Shipment<i
                                            class="fas fa-arrow-circle-right float-right"></i></a></li>
                                <li><a href="#"><i class="far fa-clock mr-3"></i>Recent Shipments<i
                                            class="fas fa-arrow-circle-right float-right"></i></a></li>
                                <li><a href="#"><i class="far fa-calendar-alt mr-3"></i>Schedule Pickup<i
                                            class="fas fa-arrow-circle-right float-right"></i></a></li>
                            </ul>
                        </div>
                        <button type="submit" class="btn btn__warning text-center btn-block">
                            <span>Sign Up</span> <i class="icon-arrow-right"></i>
                        </button>
                        <!-- /.contact-panel__form -->
                    </div>

                </div><!-- /.col-lg-3 -->
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <!-- fancybox item #3 -->
                    <div class="contact-panel">
                        <div class="content">

                            <form class="contact-panel__form" method="post"
                                action="http://7oroof.com/demos/datasoft/assets/php/contact.php" id="contactForm"
                                novalidate="novalidate">
                                <h6 class="contact-panel__title">Get A Quick Shipping Rate</h6>
                                <div class="form-group">
                                    <label for="form">Form <span style="color:red;">*</span></label>
                                    <input type="text" class="form-control" placeholder="Name" id="form" name="form"
                                        required="" aria-required="true">
                                </div>
                                <div class="form-group">
                                    <label for="to">To<span style="color:red;">*</span></label>
                                    <input type="email" class="form-control" placeholder="Email" id="to" name="to"
                                        required="" aria-required="true">
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6 col-12">
                                        <label for="forweight">Weight<span style="color:red;">*</span></label>

                                        <select class="form-control">
                                            <option value="1" selected>Inquiry</option>
                                            <option value="2">case study</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <label for="unit">Unit<span style="color:red;">*</span></label>

                                        <select class="form-control">
                                            <option value="1" selected>Kg</option>
                                            <option value="2">Gram</option>
                                        </select>
                                    </div>


                                </div>
                            </form>
                        </div>
                        <button type="submit" class="btn btn__warning text-center btn-block">
                            <span>Check Rates</span> <i class="icon-arrow-right"></i>
                        </button>


                        <!-- /.contact-panel__form -->
                    </div>

                </div><!-- /.col-lg-3 -->
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <!-- fancybox item #2 -->
                    <div class="contact-panel">
                        <div class="content">

                            <h6 class="contact-panel__title">Find Evaanno</h6>
                            <p class="text-warning text-center">in Bangladesh</p>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3746840.957583279!2d88.10067349364856!3d23.495631602269377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30adaaed80e18ba7%3A0xf2d28e0c4e1fc6b!2sBangladesh!5e0!3m2!1sen!2sbd!4v1624779224715!5m2!1sen!2sbd"
                                width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                        <button type="submit" class="btn btn__warning text-center btn-block">
                            <span>Find nearest location</span> <i class="icon-arrow-right"></i>
                        </button>
                        <!-- /.contact-panel__form -->
                    </div>
                </div><!-- /.col-lg-3 -->


            </div><!-- /.row -->
        </div>

    </section>


    <!-- ========================
      ////News
    =========================== -->
    <section class="banner-2 py-2">
        <div class="row no-gutters">
            <!-- <div class="col-12 col-lg-12">
                                >
            </div> -->
            <div class="col-12 col-lg-4">
                <div class="banner-img-area-top">
                    <img src="assets/images/services/1.webp" alt="" class="img-fluid ban-img-t">
                    <div class="ben-img-content">
                        <span><i class="fas fa-briefcase"></i></span>
                        <h6>Small Bussiness</h6>
                        <p class="w-md-75 w-lg-50 w-75">We are seamlessly serving f-commerce and e-commerce businesses
                            through our platform</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="banner-img-area-top">
                    <img src="assets/images/services/2.webp" alt="" class="img-fluid ban-img-t">
                    <div class="ben-img-content">
                        <span><i class="fas fa-user-lock"></i></span>
                        <h6>Individuals</h6>
                        <p class="w-md-75 w-lg-50 w-75"> We serve Person 2 Person (P2P) delivery needs across Bangladesh
                            .</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="banner-img-area-top">
                    <img src="assets/images/services/3.webp" alt="" class="img-fluid ban-img-t">
                    <div class="ben-img-content">
                        <span><i class="fas fa-shipping-fast"></i></span>
                        <h6>Enterprises</h6>
                        <p class="w-md-75 w-lg-50 w-75">We have moved more than 100,000 tonnes for the biggest
                            enterprises in Bangladesh</p>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <section class="banner-layout2 py-0 bg-white text-dark">
        <div class="container-fluid px-0">
            <div class="row row-no-gutter align-items-center">
                <div class="col-sm-12 col-md-12 col-lg-7">
                    <div class="banner-text inner-padding">
                        <div class="heading-layout2 mb-20">
                            <h3 class="heading__title mb-10">Choose EVAANNO as your delivery partner</h3>
                            <p class="heading__desc">EVAANNO aims to aid Bangladesh’s expanding e-commerce sector by
                                providing tech-first logistics support. With exclusive features and a talented
                                workforce, EVAANNO gives the delivery service industry of Bangladesh a brand new pace.
                            </p>
                        </div><!-- /.heading -->
                        <div class="row fancybox-light">
                            <div class="col-sm-6">
                                <!-- fancybox item #1 -->
                                <div class="fancybox-item">
                                    <div class="fancybox__icon">
                                        <i class="icon-website"></i>
                                    </div><!-- /.fancybox__icon -->
                                    <div class="fancybox__content">
                                        <h4 class="fancybox__title">
                                            Next day payment</h4>
                                        <p class="fancybox__desc">Once the delivery is complete you will receive payment
                                            the very next day.</p>
                                    </div><!-- /.fancybox-content -->
                                </div><!-- /.fancybox-item -->
                            </div><!-- /.col-lg-3 -->
                            <div class="col-sm-6">
                                <!-- fancybox item #1 -->
                                <div class="fancybox-item">
                                    <div class="fancybox__icon">
                                        <i class="icon-website"></i>
                                    </div><!-- /.fancybox__icon -->
                                    <div class="fancybox__content">
                                        <h4 class="fancybox__title">

                                            Best COD rates</h4>
                                        <p class="fancybox__desc">COD charge inside Dhaka 0%, outside Dhaka 1%.</p>
                                    </div><!-- /.fancybox-content -->
                                </div><!-- /.fancybox-item -->
                            </div><!-- /.col-lg-3 -->
                            <div class="col-sm-6">
                                <!-- fancybox item #1 -->
                                <div class="fancybox-item">
                                    <div class="fancybox__icon">
                                        <i class="icon-website"></i>
                                    </div><!-- /.fancybox__icon -->
                                    <div class="fancybox__content">
                                        <h4 class="fancybox__title">Payment via bKasht</h4>
                                        <p class="fancybox__desc">Merchants can accept payment via bank or bKash..</p>
                                    </div><!-- /.fancybox-content -->
                                </div><!-- /.fancybox-item -->
                            </div><!-- /.col-lg-3 -->
                            <div class="col-sm-6">
                                <!-- fancybox item #1 -->
                                <div class="fancybox-item">
                                    <div class="fancybox__icon">
                                        <i class="icon-website"></i>
                                    </div><!-- /.fancybox__icon -->
                                    <div class="fancybox__content">
                                        <h4 class="fancybox__title">
                                            Reverse Delivery</h4>
                                        <p class="fancybox__desc">We provide reverse delivery, allowing customers to
                                            return products.</p>
                                    </div><!-- /.fancybox-content -->
                                </div><!-- /.fancybox-item -->
                            </div><!-- /.col-lg-3 -->
                            <div class="col-sm-6">
                                <!-- fancybox item #1 -->
                                <div class="fancybox-item">
                                    <div class="fancybox__icon">
                                        <i class="icon-website"></i>
                                    </div><!-- /.fancybox__icon -->
                                    <div class="fancybox__content">
                                        <h4 class="fancybox__title">
                                            Secure Handling</h4>
                                        <p class="fancybox__desc">Compensation policy guarantees safety of your
                                            shipment.</p>
                                    </div><!-- /.fancybox-content -->
                                </div><!-- /.fancybox-item -->
                            </div><!-- /.col-lg-3 -->
                            <div class="col-sm-6">
                                <!-- fancybox item #2 -->
                                <div class="fancybox-item">
                                    <div class="fancybox__icon">
                                        <i class="icon-Primary"></i>
                                    </div><!-- /.fancybox__icon -->
                                    <div class="fancybox__content">
                                        <h4 class="fancybox__title">
                                            Live tracking</h4>
                                        <p class="fancybox__desc">Check the real-time location and status of your parcel
                                            from anywhere.
                                        </p>
                                    </div><!-- /.fancybox-content -->
                                </div><!-- /.fancybox-item -->
                            </div><!-- /.col-lg-3 -->
                        </div><!-- /.row -->
                    </div>
                </div><!-- /.col-xl-6 -->
                <div class="col-sm-12 col-md-12 col-lg-5">
                    <div class="banner-img">
                        <img src="assets/images/gallery/team.jpg" alt="banner" class="img-fluid text-center">
                    </div>
                </div><!-- /.col-xl-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
    <section class="single-banne my-3 py-0" style="background-image: url('assets/images/banners/demo.jpg');">
        <div class="row">
            <div class="col-md-8 col-12"></div>
            <div class="col-md-4 col-12">
                <div class="sb-bg px-2 py-4">
                    <h5>Our Divisions</h5>
                    <p>DHL connects people in over 220 countries and territories worldwide. Driven by the power of more
                        than 400,000 employees, we deliver integrated services and tailored solutions for managing and
                        transporting letters, goods and information.</p>
                    <button class="btn btn-warning btn-lg">Learn more</button>
                </div>
            </div>
        </div>
    </section>
    <section class="team-layout1 text-center pb-40">
        <div class="container">
            <div class="row">
                <!-- Member #1 -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="member">
                        <div class="member__img">
                            <img src="assets/images/gallery/ilt.png" alt="member img">
                            <div class="member__hover">
                                <div class="member__content-inner">

                                </div><!-- /.member-content-inner -->
                            </div><!-- /.member-hover -->
                        </div><!-- /.member-img -->
                        <div class="member__info">
                            <h5 class="member__name">Industry-leading tech</h5>
                            <p class="member__desc">With a brilliant team of engineers driving everything we do, EVAANNO
                                offers the best possible service experience powered by superior technology</p>
                        </div><!-- /.member-info -->
                    </div><!-- /.member -->
                </div><!-- /.col-lg-4 -->
                <!-- Member #2 -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="member">
                        <div class="member__img">
                            <img src="assets/images/gallery/nc.png" alt="member img">
                            <div class="member__hover">
                                <div class="member__content-inner">

                                </div><!-- /.member-content-inner -->
                            </div><!-- /.member-hover -->
                        </div><!-- /.member-img -->
                        <div class="member__info">
                            <h5 class="member__name">Nationwide coverage</h5>
                            <p class="member__desc">EVAANNO offers the widest logistics network, covering 64 districts and
                                490+ sub districts across Bangladesh</p>
                        </div><!-- /.member-info -->
                    </div><!-- /.member -->
                </div><!-- /.col-lg-4 -->
                <!-- Member #3 -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="member">
                        <div class="member__img">
                            <img src="assets/images/gallery/fs.png" alt="member img">
                            <div class="member__hover">
                                <div class="member__content-inner">

                                </div><!-- /.member-content-inner -->
                            </div><!-- /.member-hover -->
                        </div><!-- /.member-img -->
                        <div class="member__info">
                            <h5 class="member__name">Fastest solutions</h5>
                            <p class="member__desc">Backed by an agile team and dynamic operations, we promise to find
                                the fastest solutions for your needs</p>
                        </div><!-- /.member-info -->
                    </div><!-- /.member -->
                </div><!-- /.col-lg-4 -->
            </div> <!-- /.row -->

        </div><!-- /.container -->
    </section>
    <section class="bd-map">
        <div class="bg-dark">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="p-4 mt-50">
                        <h2 class="text-white">
                            EVAANNO provides logistics support in all 64 districts and 490+ sub districts across Bangladesh
                        </h2>
                        <h6 class="text-white">
                            Whatever your need, we guarantee the fastest service all over the country
                        </h6>
                        <button class="btn btn-warning btn-lg">Learn More</button>
                    </div>

                </div>
                <div class="col-12 col-md-6">
                    <img src="assets/images/gallery/bangladash-map.png" alt="" class="img-fluid py-2">
                </div>
            </div>
        </div>
    </section>
</main>
<?php
include('inc/footer.php');
?>